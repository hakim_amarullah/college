from django.test import TestCase, Client

# Create your tests here.
class TestBooksLib(TestCase):
	def test_url_exists(self):
		response = Client().get('/bookslib')
		self.assertEquals(response.status_code, 200)

	def test_page_exists(self):
		response = Client().get('/bookslib')
		self.assertTemplateUsed(response, 'booksLib.html')
	