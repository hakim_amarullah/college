from django.shortcuts import render, redirect
from .models import MataKuliah
from .forms import Subject
from Activity.models import Activity, Person
# Create your views here.
def index(request):
	matkul = MataKuliah.objects.all()
	activity = Activity.objects.all()
	return render(request, 'home.html', {'matkul': matkul, 'activities':activity})
	
def addnew(request):
	if request.method == "POST":
		form = Subject(request.POST)
		if form.is_valid():
			try:
				form.save()
				return redirect('/')
			except:
				pass
	else:
		form = Subject()
	return render (request,'add.html',{'form':form})

def edit(request, id):
	matkul = MataKuliah.objects.get(id=id)
	return render(request, 'edit.html', {'matkul': matkul})

def update(request, id):
	matkul = MataKuliah.objects.get(id=id)
	form = Subject(request.POST or None, instance=matkul)
	if form.is_valid():
		form.save()
		return redirect('/')
	else:
		matkul = MataKuliah.objects.get(id=id)
		return render (request, 'edit.html',{'matkul':matkul})


def destroy(request, id):
	matkul = MataKuliah.objects.get(id=id)
	matkul.delete()
	return redirect('/')

