from django.test import TestCase, Client

# Create your tests here.
class TestActivity(TestCase):
	def test_url_exists(self):
		response = Client().get('/activity')
		self.assertEquals(response.status_code, 200)

	def test_page_exists(self):
		response = Client().get('/activity')
		self.assertTemplateUsed(response, 'activity.html')

	def test_url_add_exists(self):
		response = Client().get('/addact')
		self.assertEquals(response.status_code, 200)
	