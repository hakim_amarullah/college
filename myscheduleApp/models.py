from django.db import models

# Create your models here.
class MataKuliah(models.Model):
	nama_matkul = models.CharField(max_length=25)
	dosen = models.CharField(max_length=25)
	sks = models.CharField(max_length=25)
	deskripsi = models.CharField(max_length=25)
	term = models.CharField(max_length=25)
	ruang_kelas = models.CharField(max_length=25)

	class Meta:
		db_table = "matakuliah"
	def __str__(self):
		return self.nama_matkul