from django.test import TestCase, Client
from django.apps import apps
from .apps import AccordionConfig
class AccordionConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(AccordionConfig.name, 'accordion')
        self.assertEqual(apps.get_app_config('accordion').name, 'accordion')

class TestAccordion(TestCase):
    def test_url_status_200(self):
        response = Client().get('/accordion')
        self.assertEqual(200, response.status_code)

    def test_template_accordion(self):
       response = Client().get('/accordion')
       self.assertTemplateUsed(response, 'accordion.html')
    
    def test_view_accordion(self):
        response = Client().get('/accordion')
        html_response = response.content.decode ('utf8')
        self.assertIn("My Recent Activity", html_response)
        self.assertIn("Pengalaman Organisasi/Kepanitiaan", html_response)
        self.assertIn("Prestasi", html_response)
        self.assertIn("Hal yang ingin dicapai", html_response)